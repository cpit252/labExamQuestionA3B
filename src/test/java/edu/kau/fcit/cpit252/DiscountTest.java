package edu.kau.fcit.cpit252;

import edu.kau.fcit.cpit252.discountStrategy.ClearanceDiscount;
import edu.kau.fcit.cpit252.discountStrategy.Discount;
import edu.kau.fcit.cpit252.discountStrategy.MemberDiscount;
import edu.kau.fcit.cpit252.discountStrategy.RegularDiscount;
import edu.kau.fcit.cpit252.shopping.Product;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DiscountTest {
    @Test
    public void testMemberDiscount(){
        Discount d = new MemberDiscount();
        final double price = 4.99;
        final double rate = .20;
        final double expectedDiscountedPrice = price - (price * rate);
        Product p = new Product("key tag", "12567840", price);
        p.setPrice(d.apply(p.getPrice()));
        assertEquals(p.getPrice(), expectedDiscountedPrice, 0);
    }

    @Test
    public void testClearanceDiscount(){
        Discount d = new ClearanceDiscount();
        final double price = 4.99;
        final double rate = .50;
        final double expectedDiscountedPrice = price - (price * rate);
        Product p = new Product("key tag", "12567840", price);
        p.setPrice(d.apply(p.getPrice()));
        assertEquals(p.getPrice(), expectedDiscountedPrice, 0);
    }


    @Test
    public void testRegularDiscount(){
        Discount d = new RegularDiscount();
        final double price = 4.99;
        final double rate = .05;
        final double expectedDiscountedPrice = price - (price * rate);
        Product p = new Product("key tag", "12567840", price);
        p.setPrice(d.apply(p.getPrice()));
        assertEquals(p.getPrice(), expectedDiscountedPrice, 0);
    }
}
