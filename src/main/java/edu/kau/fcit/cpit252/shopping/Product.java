package edu.kau.fcit.cpit252.shopping;

public class Product {
    private String sku;
    private String name;
    private double price;

    public Product(String name, String sku, double price) {
        this.name = name;
        this.sku = sku;
        this.price = price;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String toString(){
        return this.name + "\t" + "SKU: " + this.sku +
                "\t price: SAR" + String.format("%.2f", this.price);
    }
}
