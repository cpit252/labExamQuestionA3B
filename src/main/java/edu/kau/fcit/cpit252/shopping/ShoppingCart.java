package edu.kau.fcit.cpit252.shopping;

import edu.kau.fcit.cpit252.discountStrategy.Discount;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    List<Product> products;

    public ShoppingCart(){
        this.products=new ArrayList<Product>();
    }

    public void addItem(Product product){
        this.products.add(product);
    }

    public void removeItem(Product product){
        this.products.remove(product);
    }

    public double calculateTotal(){
        double sum = 0;
        for(Product product : this.products){
            sum += product.getPrice();
        }
        return sum;
    }

    public void pay(){
        double amount = calculateTotal();
        System.out.println("Your total is SAR" + String.format("%.2f", amount));
    }

    public void applyDiscount(Discount discount){
        for(Product product : this.products){
            product.setPrice(discount.apply(product.getPrice()));
        }
    }

    @Override
    public String toString(){
        String message ="";
        for(Product product : this.products){
            message += product.toString() + "\n";
        }
        return message;
    }
}

