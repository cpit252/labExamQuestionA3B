package edu.kau.fcit.cpit252.discountStrategy;

public class ClearanceDiscount implements Discount {

    private final double rate = .50;

    @Override
    public double apply(double amount) {
        return amount - (amount * rate);
    }
}
