package edu.kau.fcit.cpit252.discountStrategy;

public class MemberDiscount implements Discount {

    private final double rate = .20;

    @Override
    public double apply(double amount) {
        return amount - (amount * rate);
    }
}
