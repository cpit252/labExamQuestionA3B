package edu.kau.fcit.cpit252.discountStrategy;

public interface Discount {
    public double apply(double amount);
}
