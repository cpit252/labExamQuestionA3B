package edu.kau.fcit.cpit252.discountStrategy;

public class RegularDiscount implements Discount {

    private final double rate = .05;

    @Override
    public double apply(double amount) {
        return amount - (amount * rate);
    }
}
