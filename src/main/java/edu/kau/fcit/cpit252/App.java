package edu.kau.fcit.cpit252;

import edu.kau.fcit.cpit252.discountStrategy.ClearanceDiscount;
import edu.kau.fcit.cpit252.discountStrategy.MemberDiscount;
import edu.kau.fcit.cpit252.discountStrategy.RegularDiscount;
import edu.kau.fcit.cpit252.shopping.Product;
import edu.kau.fcit.cpit252.shopping.ShoppingCart;

import java.util.Scanner;

public class App
{
    public static void main( String[] args ){
        ShoppingCart cart = new ShoppingCart();

        Product item1 = new Product("Body wash", "549032", 10.99);
        Product item2 = new Product("Shampoo", "872150",14.99);

        cart.addItem(item1);
        cart.addItem(item2);

        System.out.println("Shopping cart:\n"+ cart.toString());
        System.out.println("Your total is: $" + cart.calculateTotal());
        System.out.println("\nPlease select discount type:\n" +
                "1. Clearance\n2. Member\n3. Regular\n");
        Scanner scanner = new Scanner(System.in);
        int discountType = scanner.nextInt();
        if (discountType == 1){
            cart.applyDiscount(new ClearanceDiscount());
        }
        else if(discountType == 2){
            cart.applyDiscount(new MemberDiscount());
        }
        else if(discountType == 3){
            cart.applyDiscount(new RegularDiscount());
        }
        else {
            System.out.println("Invalid discount type");
            return;
        }
        System.out.println("Shopping cart after applying discount:\n"+ cart.toString());
        cart.pay();
    }
}
