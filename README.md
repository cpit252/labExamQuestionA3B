### Lab exam question A3B:
A store is offering different types of discounts to its customers (regular discount, member only discount, clearance discount). Implement this scenario using the strategy design pattern.
### Building, testing, running

```
mvn compile;
mvn test;
mvn compile exec:java -Dexec.mainClass="edu.kau.fcit.cpit252.App";
```


